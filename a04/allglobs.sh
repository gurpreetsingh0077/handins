#!/bin/sh


if [ $# == 0 ] ; then
     echo no argument passed
     exit 1;
fi
str=$(echo "$*" | tr -d '[:space:]')

while read line ; do
     [[ $line == $str ]] && echo $line
done

