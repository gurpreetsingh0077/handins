#!/bin/sh


hasDupes(){
   if cmp -s "$1" "$2"  ; then
         return 0
      fi
      return 1
}


if [ $# -eq 0 ] ; then
   for file in $(ls) ; do
       echo $file
   for i in $(ls) ; do
      if [ "$file" != "$i" ] ; then
        if hasDupes $file $i  ; then
         echo "$file" : "$i"
      fi
   fi
    done
 done

 fi
 
 if [ $# -gt 0] ; then
 for i in $a ; do
 if [ -f $i ] ; then
   if hasDupes $i $a; then
   echo $i :$a
 
    fi
    fi
 done
 exit 2
 fi
 