#!/bin/sh

if [ $# -eq 1 ] ; then

for i in $(seq 1 $1) ; do
   ./child.sh & 
    pids+=" $!"
done


for i in $pids ; do 
    if wait $i ; then
       success+=" $i"
        break
    else 
       failure+=" $i"
       
     fi
done
  
 if [ -z "$success" ] ; then
 echo All Failed
 exit 1
 fi
 
 for l in $pids ; do
if ps -p $l > /dev/null
then
   kill -9 $l
   echo $l killed
fi
done 
 
 
else 
 echo error
   exit 2
fi