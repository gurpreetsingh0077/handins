#!/bin/sh

if [ $# -eq 1 ] ; then

for i in $(seq 1 $1) ; do
   ./child.sh & 
    pids+=" $!"
done

for i in $pids ; do 
    if wait $i ; then
    
       success+=" $i"
     
    else 
       failure+=" $i"
     fi
done
 echo Success : $success
 echo Failure : $failure
  
 [ -z "$failure" ] && exit 0 || exit 1
 
 
else 
 echo error
   exit 2
fi