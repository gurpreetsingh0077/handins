
Do not include name or student ID.

Submit via your handin repository on gitlab before the deadline.
Ensure that you have submitted the repository's URL via the d2l quiz.

Deadline: 2019-09-25 23:55:00-07:00
Late Deadline: 2019-09-26 23:55:00-07:00

----- Grading -----
[/50] TOTAL
	[/15] q1-4
	[/27] q5
	[/08] q6
	[/-5] not a plain text file
	[/-5] repository not private
	[/-5] instructor not added as developer on project
	[/-5] includes name, student id, or other identifying info
	[/-10] not submitted via repository
	[/20%] late penalty (24 hours)
--------------------

For all of these questions, you may NOT use:
	grep, sed, awk, or perl
	if, for, while, until, case, or read


Questions #4 onwards end with some digits in square brackets.  Those digits represent the starting digits of the shasum of the correct output.  For those questions, provide BOTH the command, and the resulting shasum of the output as in question 0 below.

q00) [0] Give a command to replace all of the capital X characters with lower case x characters in the books corpus. [e]

	cat * | tr 'X' 'x' | shasum
	e424cc5e4fae61474f7a7622b2daeb2340999602  -

--------------------


q01) [4] Suppose there are multiple copies of the program "foo" on your machine: one in the current directory, one in your home directory, one in /usr/bin, and one in /bin.
	1) Without any additional information, which one will run if you simply type "foo" on the command line?
       one in the current directory
	2) Give the command to run the one in the current directory?
       ./foo
	3) Give the command to run the one in your home directory?
        ~/foo
	4) Give the command to run the one in /usr/bin?
        /usr/bin/foo


q02) [3] For each of the following commands, state:
	i) which program opens and reads the input file, and
	ii) how shasum receives the file's contents.

	a)	shasum Meditations.txt
		i)
		ii)
	b)	cat Meditations.txt | shasum
		i)
		ii)
	c) shasum < Meditations.txt
		i)
		ii)


q03) [4] give a command to display the reverse ordering of the elements in the path variable (colon delimited). eg: "1a:2b:3c:4d" -> "4d:3c:2b:1a".
                   



q04) [4] give a set of commands that will create a file "foo" that contains 128 'y' characters followed by 128 'n' characters.  The file should contain only those 256 characters. [6]

                  printf 'y\n%.0s' {1..128} >> foo
                  printf 'n\n%.0s' {1..128} >> foo


q05) [27] for the purpose of this question a "word" is defined to be a contiguous sequence of alpha-numeric characters. Answer all questions w/r/t the books corpus (wget http://mylinux.langara.ca/~jhilliker/1280/books.tar.bz2 ; tar -xf books.tar.bz2). Give a command to:

	a) [1] remove all blank lines. "Blank," for this question, means "doesn't contain any characters." [0]
         
        cat * |tr -s '\n' | shasum

	b) [2] remove the last character of every line of the books corpus. [d]
           rev *| cut -c 2- | rev | shasum 


	c) [2] determine how often the letter 'u' or 'U' appear in the books corpus. [1]

         cat * |tr '[:upper:]' '[:lower:]'|tr -cd 'u'| wc -c

	d) [2] display the total word count (number of words, including duplicates). Display only this number. [c]
         
           cat *|tr '[:punct:]' ' '|tr -s '[:alnum:]'|wc -w

	e) [4] display the number of unique words (the word count, excluding duplicates), case insensitive (use tr and posix classes). [4]
   
         cat *|tr '[:punct:]' ' '|tr -s '[:alnum:]'|tr '[:lower:]' '[:upper:]'|uniq -u|wc -w
         
	f) [4] Display the number of words that occur only once, case insensitive. [a]

          cat *|tr '[:punct:]' ' '|tr -s '[:alnum:]'|tr '[:lower:]' '[:upper:]'|uniq -u|wc -w

	g) [4] display the name of the file in the books corpus that contains the fewest newline characters. [0]

           ls *| cat *| tr -cd '\n'|wc -w


	h) [8] display the 10 most frequently occurring words (case insensitive) in descending order (most frequent first), one per line. Display only those words (no counts). Display them in lower case. [a]

        


q06) [8] w/r/t http://mylinux.langara.ca/~jhilliker/1280/pokemon.csv

	a) [7] give a command to display the most common type of pokemon (fields 3&4)? If a pokemon has 2 types, count both. [b]
		eg: lines 5&6 would be counted as: 2 Fire, 1 Flying.



	b) [1] how often does that type of pokemon occur? [6]
